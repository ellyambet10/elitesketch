class Product < ApplicationRecord
  has_many :pictures, dependent: :destroy
  belongs_to :service

  validates :category, presence: true
  validates :title, presence: true, length: { minimum: 5 }
  validates :body, presence: true
end
