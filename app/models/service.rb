class Service < ApplicationRecord
  has_many :products, dependent: :destroy
  has_many :clients, dependent: :destroy

  validates :category, presence: true
  validates :title, presence: true, length: { minimum: 5 }
  validates :body, presence: true
end
