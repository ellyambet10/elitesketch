class Picture < ApplicationRecord
  belongs_to :product

  validates :title, presence: true, length: { minimum: 5 }
  validates :body, presence: true
end
