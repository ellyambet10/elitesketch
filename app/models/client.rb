class Client < ApplicationRecord
  belongs_to :service

  validates :category, presence: true
  validates :type, presence: true
  validates :email, presence: true, uniqueness: true
end
