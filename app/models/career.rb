class Career < ApplicationRecord
  validates :fullname, presence: true, length: { minimum: 5 }
  validates :email, presence: true, uniqueness: true
  validates :github_repo, presence: true, uniqueness: true
  validates :portfolio, presence: true
  validates :cover, presence: true
end
