class Contact < ApplicationRecord
  validates :username, presence: true, length: { minimum: 5 }
  validates :email, presence: true, uniqueness: true
  validates :subject, presence: true
  validates :message, presence: true
end
