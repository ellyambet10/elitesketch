class About < ApplicationRecord
  validates :title, presence: true, length: { minimum: 5 }
  validates :subtitle, presence: true
  validates :body, presence: true
end
