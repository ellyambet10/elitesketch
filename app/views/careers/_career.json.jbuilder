json.extract! career, :id, :fullname, :email, :github_repo, :portfolio, :cover, :created_at, :updated_at
json.url career_url(career, format: :json)
