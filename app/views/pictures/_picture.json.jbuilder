json.extract! picture, :id, :title, :body, :product_id, :created_at, :updated_at
json.url picture_url(picture, format: :json)
