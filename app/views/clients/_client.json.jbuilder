json.extract! client, :id, :category, :type, :email, :service_id, :created_at, :updated_at
json.url client_url(client, format: :json)
