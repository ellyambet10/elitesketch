json.extract! about, :id, :title, :subtitle, :body, :created_at, :updated_at
json.url about_url(about, format: :json)
