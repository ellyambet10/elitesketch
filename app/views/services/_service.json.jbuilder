json.extract! service, :id, :category, :title, :body, :created_at, :updated_at
json.url service_url(service, format: :json)
