Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  resources :clients
  resources :careers
  resources :contacts
  resources :abouts
  resources :homes
  resources :services
  resources :products
  resources :pictures

  root 'homes#index'
end
