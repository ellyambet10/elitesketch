# README

This is EliteSketch Website. A website dedicated to masterpiece art work & Application Development (Websites, Mobile Apps & Games).

Tools Used:

* Languages: Front-End ---->>>> HTML5, CSS, Bootstrap, JS.
			 Back-End  ---->>>> Ruby.

* Framework: Rails.

* OS: Ubuntu 19.04 (Linux).

* Database: Postgresql.

* Testing: Minitest.

* Hosting: Heroku.

* DNS provider: Namecheap.
