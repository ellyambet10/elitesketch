class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :category
      t.string :title
      t.text :body
      t.references :service, foreign_key: true

      t.timestamps
    end
  end
end
