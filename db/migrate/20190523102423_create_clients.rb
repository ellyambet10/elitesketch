class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :category
      t.string :type
      t.string :email
      t.references :service, foreign_key: true

      t.timestamps
    end
  end
end
