class CreateCareers < ActiveRecord::Migration[5.2]
  def change
    create_table :careers do |t|
      t.string :fullname
      t.string :email
      t.string :github_repo
      t.string :portfolio
      t.text :cover

      t.timestamps
    end
  end
end
